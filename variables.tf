variable "region" {}
variable "gcp_project" {}
variable "credentials" {}
variable "name" {}
variable "ssh_key" {}
variable "zone" {}
variable "machine_type" {}
variable "source_image" {}
variable "consul_cluster_name" {}
variable "vault_cluster_name" {}
variable "network_name" {}

variable "vault_instances" {
  default = "3"
}

variable "consul_instances" {
  default = "5"
}
