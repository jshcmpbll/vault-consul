#! /bin/bash
set -x

unzip /tmp/consul.zip -d /usr/bin/

useradd consul --no-create-home

mkdir -p /etc/consul/conf.d
chown consul /etc/consul/conf.d
chown consul /etc/consul
mkdir -p /var/consul
chown consul /var/consul
mkdir /var/lib/consul
chown /var/lib/consul


cat > "/etc/systemd/system/consul.service" <<EOF
[Unit]
Description=consul
Wants=network.target
After=network.target

[Service]
Environment="GOMAXPROCS=2" "PATH=/usr/local/bin:/usr/bin:/bin"
ExecStart=/usr/bin/consul agent -config-file=/etc/consul/consul.json -config-dir=/etc/consul/conf.d
ExecReload=/bin/kill -HUP $MAINPID
KillSignal=TERM
User=consul
WorkingDirectory=/var/lib/consul

[Install]
WantedBy=multi-user.target
EOF

cat > "/etc/consul/consul.json" <<EOF
{
  "advertise_addr": "$(hostname -i)",
  "advertise_addr_wan": "$(hostname -i)",
  "bind_addr": "$(hostname -i)",
  "bootstrap_expect": 5,
  "client_addr": "0.0.0.0",
  "data_dir": "/var/consul",
  "ports": {
    "dns": 8600,
    "http": 8500,
    "serf_lan": 8301,
    "serf_wan": 8302,
    "server": 8300
  },
  "server": true,
  "retry_join" : ["jcampbell-consul-1","jcampbell-consul-2","jcampbell-consul-3","jcampbell-consul-4","jcampbell-consul-5"],
  "verify_incoming": false,
  "verify_outgoing": false,
  "telemetry": {
    "prometheus_retention_time":"30s",
    "disable_hostname": true
  }
}
EOF

systemctl daemon-reload
systemctl start consul
sleep 5
systemctl status --no-pager consul
sleep 10
consul members

